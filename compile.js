"use strict";
require("colors");
const ARCH = { x64: 1, ia32: 0 },
	PLATFORMS = ["win!x64",  "win!ia32", "linux!x64", "linux!ia32"],
	builder = require("electron-builder"),
	path = require("path"),
	Platform = builder.Platform,
	iconDir = path.resolve("./app/app-icons"),
	spinner = path.resolve("./app/spinner.gif"),
	productName = "GIS";


// Promise is returned
require("rmdir")("./bin", (err) => {
	if(err) console.error(err);

	PLATFORMS.forEach((platform) => {
		const conf = platform.split("!");

		setTimeout(() => {
			builder.build({
				targets: Platform.fromString(conf[0]).createTarget(null, ARCH[conf[1]]),
				devMetadata: {
					build: {
						appId: "lntu.idc2016",
						copyright: "Copyright © 2016 Pshonyuk V. V.",
						compression: "maxium",
						directories: {
							app: "./app",
							output: `./bin/${conf[0]}_${conf[1]}`,
							buildResources: `./bin/${conf[0]}_${conf[1]}`
						},
						linux: {
							target:  "deb",
							category: "education",
							executable: productName.toLowerCase(),
							desktop: {
								Type: "Application",
								Name: productName,
								Exec: productName.toLowerCase(),
								Icon: "icon.png",
								Categories: "GTK;GNOME;Utility"
							}
						},
						win: {
							icon: path.join(iconDir, "icon.ico"),
							target: "nsis",
						},
						nsis: {
							oneClick: false,
							runAfterFinish: false
						},
						squirrelWindows: {
							msi: true,
							loadingGif: spinner
						}
					}
				}
			})
				.then(() => {
					console.log(`Success build for ${platform}!`.green);
				})
				.catch((error) => {
					console.error(`Error build for ${platform}. ${error}`.red);
				});
		}, 0);
	});
});
