<div class="container-fluid sadyby-container">
    <h1 class="title">Каталог садиб</h1>
    <div class="row">
        <div class="col-md-3">
            <ul class="regions">

            </ul>
        </div>
        <div class="col-md-9">
            <div class="items-wrapper">
                <ul class="items">
                </ul>
            </div>
        </div>
    </div>
    <div class="modal fade" id="s-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
                </div>
            </div>
        </div>
    </div>
</div>