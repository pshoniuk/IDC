<div class="tile">
	<div>
		<i class='mdi <%=icon%>'></i>
	</div>
	<a href='<%= route %>'><%=caption%></a>
	<ul class="sub-items"></ul>
</div>