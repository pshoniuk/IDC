var $ = require("jquery");
window.jQuery = $;

$(function(){
	var _ = require("underscore"),
		path = require("path"),
		_data = require('./js/source/data'),
		Controller = require("./js/source/controller"),
		readDir = require("./js/source/readDir"),
		templates = require("./js/source/templates"),
		{remote} = require('electron'),
		app, windowIcons;

	app = {};

	//get templates
	templates.add(readDir.getFiles(path.resolve(path.join(__dirname, "./templates/"))));

	window.gisRootPath = __dirname;
	windowIcons = $(document.body).find(".window-controls");

	//close app
	windowIcons.find(".close").on("click", function(){
		var win = remote.getCurrentWindow();
			win.close();
	});

	//minimize app
	windowIcons.find(".minimize").on("click", function(){
		var win = remote.getCurrentWindow();
			win.minimize(); 
	});

	//application controller
	app.controller = new Controller({
		data: _data
	});

	module.exports = app;
});