var $ = require("jquery"),
	nodePath = require("path"),
	Backbone = require("backbone"),
	templates = require("../source/templates"),
	readDir = require("../source/readDir"),
	data = require("./data.js");


function getPath(){
	return nodePath.resolve(nodePath.join.apply(nodePath, arguments));
}

module.exports = (function(){
	var app = {};
	app._public = {};
	app.models = {};
	app.views = {};
	Backbone.$ = $;

	_const = {
		"resPath"	 : "/resourses/",
		"bgExt"		 : "jpg" 
	};


	app.$ = {
		"body": $(document.body)
	};


	createSection = function(content){
		var rootTpl = templates.get("fullpage.section.tpl"),
			introTpl = templates.get("fullpage.intro.tpl"),
			$root = $(rootTpl()).
				append(introTpl({content: content}));
		app.$.body.append($root);
		return $root;
	};


	//MAIN MENU
	app.models.MainMenu = Backbone.Model.extend({});


	app.views.MainMenu = Backbone.View.extend({
		className: "menu-wrap",
		tagName: "div",
		events: {
			"click .tile": "selectItem"
		},


		initialize: function(){
			this.listenTo(this.model, {
				"change:data": this.render,
				"destroy": this.destroy
			});
			$(document.body).addClass("menu");
			this.render();
		},


		render: function(){
			var i, j, item, subItem, $item, l, l2, error,
				data = this.model.attributes.data,
				itemTpl = templates.get("mainMenu.item.tpl"),
				subItemTpl = templates.get("mainMenu.subItem.tpl");

			if(!data || typeof data !== "object"){
				return;
			}
			l = data.length;
			for(i = 0; i < l; i++){
				item = data[i];
			 	try{
					if (!item.route){
						item.route = "#";
					}
			 		$item = $(itemTpl(item));
			 		this.$el.append($item);
			 	}catch(error){
			 		item = null;
			 	}
			 	if(item && item.content){
			 		$item = $item.find(".sub-items");
			 		l2 = item.content.length;
			 		for(j = 0; j < l2; j++){
			 			subItem = item.content[j];
					 	try{
					 		$item.append(subItemTpl(subItem));
					 	}catch(error){}
			 		}
			 	}
			}
			return this.$el;
		},


		destroy: function(){
			this.stopListening();
			this.remove();
		},


		selectItem: function(e){
			var target, $item, link;
			target = $(e.target);
			link = $(e.currentTarget).find(">a");

			if(target.closest(".sub-items").length){
				$item = target.closest(".sub-item");
				hash = $item.find("a").attr("href");
				if(hash){
					window.location.hash = hash.substr(1);
				}
				return;
			}else if((hash = link.attr("href")) && hash.length > 1){
				window.location.hash = hash.substr(1);
			}


			this.$(".tile").removeClass("active");
			target.closest(".tile").addClass("active");
			return false;
		}
	});


	app._public.mainMenu = function(pointName){
		var args;
		args = Array.prototype.slice.call(arguments, 1);
		this.__proto__ = app._public.mainMenu.prototype;
		this.model = new app.models.MainMenu({data: args});
		view = new app.views.MainMenu({model: this.model});
		app.$.body.append(view.el);
		$(document.body)
			.children(".go-main")
			.hide();
		return this;
	};

	app._public.mainMenu.prototype.destroy = function(pointName){
		this.model.destroy();
		$(document.body)
			.children(".go-main")
			.show();
	};

	//background
	app._public.bg = function(pointName, bgPath){
		bgPath = bgPath || window.gisRootPath + _const.resPath + pointName + "\\bg." + _const.bgExt;
		bgPath = bgPath.replace(/\\/g, "/");
		app.$.body.css("background-image", "url(" + bgPath + ")");
	};




	//text content
	app._public.textContent = function(pointName, text){
		$(document.body).removeClass("menu");
		this.__proto__ = app._public.textContent.prototype;
		text || (text = readDir.getFile(getPath(window.gisRootPath, _const.resPath, pointName, "./textContent.tpl")));
		this.$root = createSection(text);
		this.$root
			.addClass("idc-text-content")
			.find(".intro")
			.css("height", app.$.body.height() - 100 + "px");
		return this;
	};

	app._public.textContent.prototype.destroy = function(){
		this.$root.remove();
	};



	
	$(".go-main").on("click", function () {
		window.history.back();
	});



	app._public.mainTitle = function (pointName, text) {
		this.__proto__ = app._public.mainTitle.prototype;
		this.$root = $("<h1 class='app-main-title'><a href='#content/navMenu'>" + text + "</a></h1>");
		this.$root.on("click", function () {
			window.location.hash = "#content/navMenu";
		});
		$(document.body).append(this.$root).addClass("main-state");
		return this;
	};


	app._public.mainTitle.prototype.destroy = function(){
		this.$root.remove();
		$(document.body).removeClass("main-state");
	};



	app._public.catalog = require("./catalog");


	return app._public;
})();