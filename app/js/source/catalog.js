var $ = require("jquery"),
	_ = require("underscore"),
	nodePath = require("path"),
	bootstrap = require("../../node_modules/bootstrap/dist/js/bootstrap.min"),
	templates = require("../source/templates"),
	readDir = require("../source/readDir"),
	data = require("./data.js"),
	owlCarousel = require("../vendor/owl.carousel.min")($),
	resPath = "/resourses/";


function getPath(){
	for(var i = 0, l = arguments.length; i < l; i++) {
		arguments[i] = "" + arguments[i];
	}
	return nodePath.resolve(nodePath.join.apply(nodePath, arguments));
}


Catalog = function (pointName, data) {
	$(document.body).removeClass("menu");
	this.__proto__ = Catalog.prototype;
	this.data = data;
	this.createStructure();
	this.renderRegions();
	this.$el.data("idc.catalog", this);
	this.pointName = pointName;
	$(document.body)
		.append(this.$el)
		.children("img")
		.hide();
	$(".sadyby-container .regions li").eq(0).trigger("click");
	return this;
};


Catalog.prototype.getItemGeneralIndex = function (regionIndex, itemIndex) {
	var i, l, res = 0, d = this.data;
	l = Math.min(regionIndex, d.length);
	for(i = 0; i < l; i++){
		res += d[i].length - 1;
	}
	return res += itemIndex;
};

Catalog.prototype.createStructure = function () {
	var rootTpl = templates.get("sadyby.root.tpl");
	this.$el = $el = $(rootTpl());
	this.$regions = $el.find(".regions");
	this.$items = $el.find(".items");
	this.$modal = $el.find("#s-modal");
};


Catalog.prototype.renderRegions = function () {
	var i, l, d = this.data,
		regionTpl = templates.get("sadyby.region.tpl");
	for(i = 0, l = d.length; i < l; i++){
		this.$regions.append(regionTpl({
			index: i,
			title: d[i][0]
		}));
	}
};


Catalog.prototype.renderRegionItems = function (index) {
	var i, l, gI,
		itemPtl = templates.get("sadyby.item.tpl"),
		itemsData = this.data[index];
	this.$items.html("");

	for(i = 1, l = itemsData.length; i < l; i++){
		gI = this.getItemGeneralIndex(index, i);
		this.$items.append(itemPtl({
			index: gI,
			title: itemsData[i],
			src: getPath(window.gisRootPath, resPath, this.pointName, gI, "imgs", "preview.jpg")
		}));
	}
};


Catalog.prototype.showItem = function (index, title) {
	var i, l,
		self = this,
		mapImg= "",
		carousel = "<div class='modal-owl m-hide'>",
		path = getPath(window.gisRootPath, _const.resPath, this.pointName, index),
		text = readDir.getFile(getPath(path, "textContent.tpl")),
		owlItemTpl = templates.get("carousel.item.tpl"),
		filesList = readDir.getListFiles(getPath(path, "imgs"));


	for(i = 0, l = filesList.length; i < l; i++){
		carousel += owlItemTpl({
			url: _.escape(getPath(path, "imgs", filesList[i]))
		});
	}
	carousel += "</div>";
	mapImg = "<img  class='map-img' src='" + getPath(path, "map.png") + "' width='868px'>";

	this.$modal.find(".modal-body")[0].innerHTML = carousel + text + mapImg;
	this.$modal
		.find(".modal-title")
		.html(title);

	this.$modal
		.modal({
			show: true
		})
		.on("shown.bs.modal", function () {
			self.$modal
				.find(".modal-owl")
				.owlCarousel({
					autoplayHoverPause: true,
					autoplayTimeout: 5000,
					autoWidth: true,
					autoplay: true,
					dotsEach: true,
					margin: 10,
					items: 3,
					loop: true,
					dots: true
				}).removeClass("m-hide");
		});
};


Catalog.prototype.destroy = function () {
	this.$el.remove();
	$(document.body)
		.children("img")
		.show();
};




$(document).on("click", ".sadyby-container .regions li", function () {
	var $this = $(this),
		index = $this.attr("data-index"),
		pluginData = $this.closest(".sadyby-container").data("idc.catalog");

	$(".sadyby-container .regions li").removeClass("active");
	$this.addClass("active");
	pluginData.renderRegionItems(index);
});


$(document).on("click", ".sadyby-container .items li", function () {
	var index = $(this).attr("data-index"),
		pluginData = $(this).closest(".sadyby-container").data("idc.catalog");
	pluginData.showItem(index, $(this).find(">p").text());
});


module.exports = Catalog;