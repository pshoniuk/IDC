var app = {},
	$ = require("jquery"),
	_ = require("underscore"),
	_ext = require("../source/extends"),
	Backbone = require("backbone");

app._public = {};
app._private = {};

app._private.getFirstKey = function(obj) {
	var key;
	for(key in obj){
		if(obj.hasOwnProperty(key)){
			return key;
		}
	}
	return null;
};


app._private.Router = Backbone.Router.extend({
	initialize: function(args){
		this.model = args.model;
	},

	routes: {
		"content/:pointName": "changeContent",
		"main"				: "main",
		""					: "main"
	},

	main: function  (argument) {
		this.changeContent("main");
	},

	changeContent: function(pointName) {
		if(this.model){
			this.model.set("point", pointName);
		}
	}
});


app._public.Controller = Backbone.Model.extend({
	initialize: function(){
		this.goToPoint("main");
		this.listenTo(this, {
			"change:point": this.changePoint
		});
		this.router = new app._private.Router({model: this});
		Backbone.history.start();
	},


	changePoint: function(model, pointName){
		this.goToPoint(pointName);
	},


	getPoint(pointName){
		var attrs;
		attrs = this.attributes;
		if (!attrs.data || typeof attrs.data !== "object"){
			return null;
		}
		return attrs.data[pointName];
	},


	goToPoint: function(pointName){
		var point, i, driver, key, argsDriver, drivers, obj;
		drivers = this.get("drivers");
		point = this.getPoint(pointName);

		if(!point)return;

		if(drivers){
			for(i = 0; i < drivers.length; i++){
				if(drivers[i] && drivers[i].destroy){
					drivers[i].destroy();
				}
			}
		}

		drivers = [];
		for(i = 0; i < point.length; i++){
			key = app._private.getFirstKey(point[i]);
			if(!key) continue;
			argsDriver = [pointName].concat(point[i][key]);
			driver = _ext[key];
			if(argsDriver && typeof driver === "function"){
				drivers.push(driver.apply({}, argsDriver));
			}
		}

		this.set("drivers", drivers);
	}

});

module.exports = app._public.Controller;