const electron = require("electron"),
	path = require("path"),
	iconPath = path.resolve(path.join(__dirname, "app-icons/icon.png")),
	{ BrowserWindow, app } = electron;

let mainWindow = null;


app.on("window-all-closed", function() {
	if (process.platform != "darwin") app.quit();
});

app.on("ready", function() {
	mainWindow = new BrowserWindow({
		"kiosk" : true,
		"icon": iconPath
	});
	mainWindow.loadURL("file://" + __dirname + "/index.html");
	mainWindow.on("closed", function() {
		mainWindow = null;
	});
});