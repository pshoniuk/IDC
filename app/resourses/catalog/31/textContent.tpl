<p class="info">Сірук Лідія Антонівна

</p>
<p class="info">Ківерцівський район</p>
<p class="info">0501046618</p>
<p class="info">(0332)5-55-42</p>



<hr>
<p>Пропонуємо Вам дві двомісні кімнати, всі побутові зручності, смачні страви із екологічно чистих продуктів.</p>
<p>Над плесами широкоплинного Стиру, за 8 км від обласного центру м. Луцька, знаходиться гостинна садиба "У Лідії”.</p>
<p>Пробігтися босоніж по ранковій росі, зловити на своєму обличчі перші сонячні промені, позасмагати, покататися на бричці чи пішки пройтися лісом. Назбирати повний кошик грибів, ягід, лікарських рослин, просто подихати чистим повітрям і насолодитися мальовничими пейзажами нашої матінки Землі – чого ще бажати душі.</p>
<p>Можна поласувати медом прямо з пасіки. Також з Вами поділяться таємницями виготовлення молокопродуктів та страв української кухні. А місцеві народні умільці познайомлять Вас із народними ремеслами.</p>
<p>Організую екскурсії по визначних місцях Волині: м. Луцьк де ви зустрінетесь із старовинними лицарями в замку князя Любарта, с. Колодяжне – до музею Лесі Українки.</p>
<p>Запрошуємо завітати у чарівне місце на Волині</p>
















