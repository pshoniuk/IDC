<p class="info">Лариса та Юрій

</p>
<p class="info">Шацький район</p>
<p class="info">0951329800</p>
<p class="info">0949081800</p>
<p class="info">info@vrt.lutsk.ua</p>
<p class="info">ICQ: 488585637 </p>
<p class="info">skype: svit96 </p>


<hr>
    <p>Будинок основний №1 (цегляний): одноповерховий, 3 окремі кімнати на 2-х та 3-х чоловік, зал, кухня, тераса</p>
    <p>Будинок №2 (цегляний): одноповерховий, 2 номери на 2-х та 3-х чоловік, тераса, санвузол та душова кімната</p>
    <p>Будинок №3 (двоповерховий з терасами) для літнього проживання туристів – гостей (відпочиваючих), кондиціонери, санвузли та душові кабінки, номери на 2-х, 3-х та 4-х людей. Є номер на 1-му поверсі на 2-х осіб з кухнею.</p>
    <p>Зручності:</p>
        <ol>
            <li>Санвузли (умивальник, туалет та душеві кабінки) окремо на один номер або на два номери. Все міського типу з каналізацією</li>
            <li>Водопровід холодної й гарячої води – цілодобово</li>
            <li>На території садиби ростуть декоративні дерева, є урожайний сад. З обох терас із 2-го поверху відкривається вид на озеро</li>
            <li>Мангали для шашликів або для запікання риби</li>
            <li>У кожній кімнаті є супутникове телебачення</li>
            <li>Декілька обладнаних кухонь і їдальні з холодною та гарячою водою</li>
            <li>Є холодильне та морозильне устаткування</li>
            <li>На маєтку налагоджено трьох разове харчування – ціни тут: www.svitiaz.ukrapk.com</li>
            <li>Є місце для стоянки автомобілів (огороджена територія без доступу сторонніх осіб).</li>
        </ol>











