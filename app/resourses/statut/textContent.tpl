<h1>
	Статут
</h1>

<div class="body">

	<h3>
		ГРОМАДСЬКОЇ ОРГАНІЗАЦІЇ
	</h3>
	<h3>
		«СПІЛКА СПРИЯННЯ РОЗВИТКУ СІЛЬСЬКОГО ЗЕЛЕНОГО ТУРИЗМУ В УКРАЇНІ»
	</h3>

	<ol>
		<h4>
			І. ЗАГАЛЬНІ ПОЛОЖЕННЯ
		</h4>
		<li>ГРОМАДСЬКА ОРГАНІЗАЦІЯ «СПІЛКА СПРИЯННЯ РОЗВИТКУ СІЛЬСЬКОГО ЗЕЛЕНОГО ТУРИЗМУ В УКРАЇНІ» (далі – Спілка) є добровільною всеукраїнською громадською організацією для досягнення мети, визначеної Статутом.</li>

		<li>Спілка діє відповідно до Конституції, Цивільного кодексу, Закону України «Про громадські об'єднання», інших актів законодавства України та Статуту.
			<p> Спілка діє на засадах законності, спільних інтересів і рівності прав членів, прозорості, публічності, добровільності та самоврядування.
		</li>

		<li>Спілка має повне найменування:
			<ol>
				<li>українською мовою: ГРОМАДСЬКА ОРГАНІЗАЦІЯ «СПІЛКА СПРИЯННЯ РОЗВИТКУ СІЛЬСЬКОГО ЗЕЛЕНОГО ТУРИЗМУ В УКРАЇНІ»</li>
				<li>англійською мовою:«UNION FOR PROMOTION OF RURAL GREEN TOURISM DEVELOPMENT IN UKRAINE»</li>
				<li>російською мовою: «СОЮЗ СОДЕЙСТВИЯ РАЗВИТИЮ СЕЛЬСКОГО ЗЕЛЕНОГО ТУРИЗМА В УКРАИНЕ»</li>
			</ol>
		</li>

		<li>Спілка має право використовувати скорочене найменування:
			<ol>
				<li>українською мовою: СПІЛКА СЗТ України;</li>
				<li>англійською мовою: UNION RGT in Ukraine;</li>
				<li>російською мовою: СОЮЗ СЗТ Украины.</li>
			</ol>
		</li>

		<li>Спілка є юридичною особою після її державної реєстрації згідно з законодавством України. Спілка від власного імені набуває майнові та особисті немайнові права, бере на себе зобов'язання, є стороною в судах та третейських судах в Україні або в юрисдикційних органах інших держав.</li>
		<li>Спілка має відокремлене майно і самостійний баланс. Спілка відкриває рахунки в національній та іноземних валютах в банківських установах в установленому законодавством порядку.</li>
		<li>Спілка має печатку та штампи, що містять її найменування та ідентифікаційний код.
			<p>Символіка Спілки реєструється в установленому законодавством порядку.</p>
			<p>Спілка може мати інші реквізити, зразки яких затверджує її виконавчий орган.</p>
		</li>

		<h5>
			ІІ. МЕТА І ЗАВДАННЯ
		</h5>

		<li> Метою Спілки є задоволення суспільних економічних, соціальних, культурних та екологічних інтересів шляхом сприяння розвитку сільського зеленого туризму.
			<p>Метою діяльності Спілки не є одержання та розподіл прибутку.</p>
		</li>

		<li>Основними завданнями (напрямами діяльності) Спілки є:
			<ol>
				<li> популяризація сільського зеленого туризму;</li>
				<li> сприяння розвитку сільської туристичної інфраструктури;</li>
				<li> популяризація звичаїв сільської гостинності, пов`язаної із нею культурної спадщини;</li>
				<li> сприяння підвищенню зайнятості сільського населення;</li>
				<li> сприяння розробці стандартів та підвищенню якості послуг у сфері сільського зеленого туризму;</li>
				<li>  надання дорадчих послуг у сфері сільського зеленого туризму;</li>
				<li> сприяння міжнародній співпраці у сфері сільського зеленого туризму.</li>
			</ol>
		</li>

		<li> Для досягнення статутної мети і виконання статутних завдань у порядку, встановленому законодавством, Спілка, зокрема, має право:
			<ol>
				<li> укладати договори та виступати учасником інших цивільно-правових відносин;</li>
				<li> надавати інформаційну, консультаційну, правову та іншу допомогу своїм членам та іншим особам, необхідну для виконання статутних завдань;</li>
				<li> брати участь у розробці та виконанні місцевих, національних і міжнародних програм і проектів, пов’язаних із виконанням статутних завдань;</li>
				<li> встановлювати і розвивати міжнародні зв’язки, співпрацю та обмін досвідом;</li>
				<li> створювати відокремлені підрозділи, бути учасником об’єднань, що сприяють виконанню статутних завдань Спілки;</li>
				<li> бути суб’єктом інформаційних правовідносин, пропагувати ідеї, мету, завдання, найменування і символіку Спілки;</li>
				<li> засновувати у встановленому законом порядку засоби масової інформації, бути суб’єктом видавничої справи без мети одержання прибутку;</li>
				<li> засновувати підприємства, організації та установи (заклади) для виконання статутних  завдань;</li>
				<li> брати участь в організації конференцій, семінарів та інших просвітніх, освітніх, благодійних заходів, що стосуються статутних завдань Спілки;</li>
				<li> вносити пропозиції, скарги та інші звернення в органи державного управління та органи місцевого самоврядування;</li>
				<li> брати участь у розробці, громадському обговоренні, експертизі та моніторингу рішень та нормативно-правових актів, що стосуються мети і статутних завдань Спілки;</li>
				<li> представляти інтереси членів Спілки при взаємодії з органами державної влади, місцевими органами виконавчої влади, а також органами місцевого самоврядування.</li>
			</ol>
		</li>

		<li>Спілка може здійснювати інші права, передбачені законодавством.</li>

		<h5>
			ІІІ. ЧЛЕНИ СПІЛКИ. ПРАВА ТА ОБОВ'ЯЗКИ. ПРИПИНЕННЯ ЧЛЕНСТВА
		</h5>

		<li>Членами Спілки можуть бути громадяни України, іноземці та особи без громадянства, які досягли 16-річного віку та сприяють виконанню статутних завдань.</li>
		<li>Правління Спілки приймає рішення щодо прийняття особи в члени Спілки та вносить відповідний запис у Реєстр членів Спілки протягом 30 днів після одержання письмової заяви, в якій особа підтверджує зобов'язання виконувати Статут, а також згоду на обробку своїх персональних даних відповідно до Статуту та законодавства.
			<p>Правління Спілки має право відмовити у прийнятті особи в члени Спілки.</p>
			<p>Правління Спілки має право делегувати право прийняття в члени Спілки відокремленим підрозділам або іншим органам Спілки.</p>
		</li>

		<li>Члени Спілки мають право:
			<ol>
				<li> брати участь у статутній діяльності та заходах Спілки;</li>
				<li> брати участь у Конференції, обирати і бути обраними до керівних органів Спілки;</li>
				<li> вносити пропозиції, заяви та скарги на розгляд керівних органів;</li>
				<li> доручати Спілці представництво своїх прав і законних інтересів, пов’язаних із метою і статутними завданнями Спілки, отримувати правову допомогу;</li>
				<li> отримувати інформацію та пояснення стосовно діяльності Спілки, за умови захисту конфіденційної інформації та персональних даних;</li>
				<li> мати доступ до фінансових та інших звітів Спілки;</li>
				<li> добровільно припиняти членство в Спілці.</li>
			</ol>
		</li>

		<li> Члени Спілки зобов'язані:
			<ol>
				 <li>виконувати вимоги Статуту і рішення керівних органів, які пов’язані з виконанням статутних завдань Спілки;</li>
				 <li>сприяти виконанню статутних завдань Спілки;</li>
				 <li>пропагувати ідеї, мету, статутні завдання і діяльність Спілки;</li>
				 <li>виконувати вимоги керівних органів Спілки щодо порядку та умов використання персональних даних та іншої інформації, яка є конфіденційною.</li>
			</ol>
		</li>
		<li> Правління має право приймати рішення про прийняття інших осіб, які сприяли виконанню статутних завдань Спілки протягом не менш ніж 12 місяців, в асоційовані члени або почесні члени Спілки на підставі рекомендації принаймні двох членів Спілки. Асоційовані та почесні члени мають право брати участь у Конференції з правом дорадчого голосу.
		</li>
			<p>Правління має право встановлювати інші відзнаки і почесні звання для осіб, які сприяли виконанню статутних завдань Спілки.</p>
		<li>Припинення членства в Спілці відбувається на підставі:
			<ol>
				<li>письмової заяви члена;</li>
				<li>рішення Правління або уповноваженого ним органу.</li>
			</ol>
			<p>Припинення членства не є підставою для припинення або невиконання будь-яких зобов’язань відповідно до цивільно-правових чи трудових договорів.</p>
		</li>

		<li> Правління або уповноважений ним орган приймає рішення про виключення члена Спілки у випадку:
			<ol>
				<li> неодноразового порушення  вимог Статуту;</li>
				<li> вчинення дій або бездіяльності, несумісних із метою Спілки;</li>
				<li> вчинення дій або бездіяльності, що завдають майнову або немайнову шкоду Спілці;</li>
				<li> неучасті в діяльності Спілки протягом 12 попередніх місяців.</li>
			</ol>
		</li>
		<li> Скарги на рішення, дії або бездіяльність керівних органів, пов’язаних із набуттям та припиненням членства, правами та обов’язками членів, розглядаються черговою Конференцією. Якщо чергова Конференція уповноважить інший постійний або тимчасовий орган, скарги розглядаються протягом одного місяця після того, як особа дізналася або мала дізнатися про указані рішення, дії або бездіяльність.
		</li>
		<h5>
			ІV.  ВІДОКРЕМЛЕНІ ПІДРОЗДІЛИ
		</h5>
		<li> Відокремлені підрозділи Спілки створюються за територіальним принципом за місцем проживання, навчання або зайнятості не менш, ніж трьох членів Спілки.
		</li>
		<li>
				<p>Відокремлені підрозділи керуються у своїй діяльності Статутом Спілки.</p>
				<p>Відокремлені підрозділи Спілки не мають статусу юридичної особи.</p>
				<p>Відокремлені підрозділи мають право мати рахунки в банківських установах та печатку.</p>
		</li>

		<li>Керівники відокремлених підрозділів мають право діяти від імені Спілки на підставі довіреностей, виданих Виконавчим директором.
		</li>

		<h5>
			V. КЕРІВНІ ОРГАНИ СПІЛКИ
		</h5>
		<li> Керівними органами Спілки є: Конференція, Правління і Виконавчий директор.</li>
		<li> Керівні органи Спілки мають право затверджувати власні правила процедури (регламенти), створювати та припиняти інші постійні і тимчасові органи за напрямками діяльності, затверджувати положення про ці органи, а також обирати, призначати або заміщати членів цих органів.</li>
		<li> Члени інших постійних і тимчасових органів можуть брати участь у засіданнях керівного органу Спілки, який створив ці постійні і тимчасові органи, з правом дорадчого голосу.

				<p>Членами Правління, а також інших постійних і тимчасових органів Спілки можуть бути особи, які не є членами Спілки.</p>
		</li>

		<li> Якщо окремі члени керівного органу письмово повідомили до початку засідання керівного органу про неможливість взяти особисту участь у засіданні, вони мають право голосувати з використанням засобів зв’язку (телефон, електронна пошта, Інтернет тощо) одночасно або до моменту закінчення засідання.</li>
		<li> У випадку проведення письмового опитування рішення вважається прийнятим, якщо за нього проголосували у письмовій формі не менше 60% членів органу. Члени органу зобов’язані письмово підтвердити результати свого голосування або відмову від голосування не пізніше трьох робочих днів з дати надсилання питання.</li>
		<li> Рішення про обрання чи затвердження складу керівних органів або інших постійних і тимчасових органів Спілки, а також про дострокове припинення повноважень цих органів приймаються особистим голосуванням. Форма голосування (відкрита чи таємна) визначається рішенням Конференції.</li>

		<h5>
			VI. КОНФЕРЕНЦІЯ
		</h5>
		<li> Найвищим керівним органом Спілки є Конференція. Конференція може бути черговою або позачерговою.</li>
		<li> Правління скликає чергову Конференцію один раз протягом трьох календарних років.

				<p>Правління документально повідомляє членів Спілки про час та місце чергової Конференції не пізніше ніж за 10 робочих днів до її проведення.</p>
				<p>Чергова Конференція має право приймати рішення, якщо в голосуванні може брати участь більшість членів Спілки або уповноважених ними представників.</p>
				<p>Представники відокремлених підрозділів обираються або затверджуються рішеннями вищих керівних органів відповідних відокремлених підрозділів.</p>
		</li>
		<li> Правління скликає позачергову Конференцію на підставі власного рішення, а також письмової вимоги не менше ніж 20% членів Спілки.
				<p>Позачергова Конференція може проводитися не пізніше, ніж через 30 днів після повідомлення про її скликання, але не раніше, ніж за 60 днів до чергової Конференції.</p>
				<p>Члени Спілки, у випадку невиконання Правлінням їх спільної письмової вимоги, мають право самостійно повідомити членів Спілки про час і місце позачергової Конференції.</p>
				<p>Позачергова Конференція може розглядати тільки питання, які вказані у рішенні або у письмовій вимозі про їх скликання.</p>
		</li>

		<li> Конференція приймає рішення, указані в пунктах 1) та  9) статті 33 Статуту, а також про відчуження майна на суму не менше, ніж 25% вартості активів Спілки кваліфікованою більшістю у 75% голосів.
			<p>Конференція приймає рішення з будь-яких інших питань простою більшістю голосів членів, які беруть участь у голосуванні, якщо попередня чергова Конференція окремим рішенням не визначить необхідну кваліфіковану більшість голосів.</p>
		</li>

		<li> До виключної компетенції Конференції належить прийняття рішень про:
			<ol>
				<li> затвердження змін і доповнень до Статуту Спілки;</li>
				<li> затвердження правил процедури Конференції;</li>
				<li> затвердження основних напрямків, планів і програм діяльності Спілки;</li>
				<li> затвердження фінансових та інших звітів органів Спілки;</li>
				<li> обрання голови, заступників та членів Правління;</li>
				<li> розпорядження майном Спілки, делегування окремих повноважень іншим органам або особам;</li>
				<li> проведення незалежного фінансового або іншого аудиту Спілки;</li>
				<li> припинення Спілки.</li>
			</ol>
		</li>
		<h5>
			VII. ПРАВЛІННЯ
		</h5>
		<li> Правління є постійно діючим керівним органом Спілки в період між черговими Конференціями.

				<p>Правління обирається Конференцією у складі не менше п’ятьох членів, в тому числі Голови Правління.</p>
				<p>Члени Правління можуть бути переобрані на наступний термін.</p>
		</li>
		<li> Якщо член Правління подає письмову заяву про припинення повноважень або втрачає здатність виконувати обов'язки з інших причин, у зв’язку з чим кількість членів Правління стає меншою, ніж указано у статті 34 Статуту, Правління більшістю голосів призначає (кооптує) нового члена на строк до чергової Конференції Спілки.</li>
		<li> Правління має повноваження:
			<ol>
				<li> визначати конкретні завдання і форми діяльності Спілки згідно зі Статутом та рішеннями Конференції;</li>
				<li> затверджувати і змінювати оперативні і фінансові плани, складати річні бюджети, баланси і звіти Спілки;</li>
				<li> встановлювати порядок фінансування та інших форм реалізації статутних завдань;</li>
				<li> приймати рішення про заснування і припинення підприємств, установ, організацій;</li>
				<li> затверджувати та присуджувати почесні відзнаки Спілки;</li>
				<li> вести реєстр членів Спілки та вносити зміни до нього;</li>
				<li> створювати і припиняти відокремлені підрозділи Спілки;</li>
				<li> приймати рішення про участь Спілки в інших юридичних особах, а також в об’єднаннях юридичних осіб;</li>
				<li> проводити періодичні та спеціальні перевірки фінансової діяльності та використання активів Спілки, залучати незалежних експертів до указаних перевірок.</li>
			</ol>
		</li>
		<li> 3асідання Правління скликаються Головою Правління не рідше одного разу на три місяці, а також протягом 10 днів після одержання письмової вимоги двох членів Правління  або Виконавчого директора.
		<li> Засідання Правління правомочне, якщо вньому бере участь більшість членів Правління, які мають право ухвального голосу.

				<p>Рішення приймаються простою більшістю голосів членів Правління, які беруть участь у голосуванні, якщо Правління не визначить окремим рішенням необхідну кваліфіковану більшість.</p>
				<p>Рішення оформлюються протоколом та зберігаються Головою Правління.</p>
		</li>
		<h5>
			VIII. ВИКОНАВЧИЙ ДИРЕКТОР
		</h5>

		<li> Виконавчий директор є вищою посадовою особою Спілки та керує її поточною діяльністю відповідно до законодавства, Статуту, рішень Конференції та Правління.
		<li> Виконавчий директор призначається Правлінням. Виконавчий директор має право призначати тимчасового заступника і видавати керівникам відокремлених підрозділів або іншим особам довіреності на вчинення юридичних дій від імені Спілки.
			<p>До призначення або заміщення Виконавчого директора його повноваження здійснює Голова Правління.</p>
		</li>

		<li> Виконавчий директор має повноваження:
			<ol>

				<li> офіційно представляти Спілку без довіреності в органах державної влади, органах місцевого самоврядування, а також у відносинах з іншими особами;</li>
				<li> укладати від імені Спілки договори та інші правочини з урахуванням обмежень, встановлених Статутом або рішеннями Конференції;</li>
				<li> забезпечувати виконання рішень і доручень Конференції і Правління;</li>
				<li> відкривати і закривати рахунки Спілки в банках та інших фінансових установах, підписувати банківські та інші фінансові документи;</li>
				<li>  затверджувати штатний розклад, укладати трудові та цивільно-правові договори, звільняти працівників згідно з законодавством, видавати накази та інші обов'язкові для працівників Спілки акти і керувати їхньою діяльністю;</li>
				<li> звітувати Конференції та Правлінню про поточну діяльність Спілки;</li>
				<li> приймати рішення щодо інших поточних питань діяльності Спілки та здійснювати інші адміністративні функції, спрямовані на реалізацію статутних завдань Спілки.</li>
			</ol>
		</li>
		<li> Правління має право припинити повноваження Виконавчого директора на підставі:
			<ol>
				<li> письмової заяви Виконавчого директора;</li>
				<li> вступу Виконавчого директора на державну або іншу публічну службу;</li>
				<li> завдання рішеннями, діями або бездіяльністю Виконавчого директора майнової або немайнової шкоди Спілки.</li>
			</ol>
		</li>
		<h5>
			ІХ. ВЛАСНІСТЬ, МАЙНО ТА КОШТИ СПІЛКИ. КОНТРОЛЬ І ЗВІТНІСТЬ
		</h5>
		<li>Спілка може мати право власності, інші речові права на рухоме і нерухоме майно, кошти в національній та іноземних валютах, цінні папери, нематеріальні активи та інше майно, що не заборонено законом та сприяє статутній діяльності Спілки.</li>
		<li>Спілка має право здійснювати щодо майна і майнових прав, які перебувають у її власності або на інших речових правах, правочини, що не суперечать законодавству та Статуту. Кошти і майно Спілки використовуються для реалізації статутних завдань, оплати праці та соціальних заходів для працівників Спілки.</li>
		<li>Спілка не несе відповідальності за зобов'язаннями членів Спілки. Члени не несуть відповідальності за зобов'язаннями Спілки, якщо інше не передбачено законодавством.

				<p>Спілка не має права забезпечувати позики або кредити членам керівних органів Спілки.</p>
				<p>Конференція має право визначити інші випадки конфлікту інтересів щодо використання активів Спілки.</p>
		</li>
		<li> Джерелами формування коштів і майна Спілки можуть бути:
			<ol>
				<li> кошти і майно, що надходять безоплатно, безповоротна фінансова допомога, добровільні пожертви членів Спілки, інших осіб;</li>
				<li> пасивні доходи;</li>
				<li> дотації або субсидії з державного чи місцевих бюджетів, а також з державних цільових фондів;</li>
				<li> благодійна допомога, гуманітарна та технічна допомога;</li>
				<li> доходи від основної діяльності Спілки відповідно до Статуту та законодавства.</li>
			</ol>
		</li>
		<li> Спілка, створені нею підприємства, організації та установи ведуть оперативний і бухгалтерський облік, а також подають статистичну, фінансову та іншу звітність в порядку та обсягах, встановлених законодавством.

				<p>Спілка не рідше одного разу на рік оприлюднює звіти та іншу інформацію про джерела залучення коштів і майна для здійснення статутної діяльності та про напрямки їх використання відповідно до законодавства України.</p>
		</li>
		<h5>
			X. ВНЕСЕННЯ ЗМІН ДО СТАТУТУ
		</h5>
		<li> Зміни і доповнення до Статуту приймає чергова або позачергова Конференція відповідно до статті 32 Статуту.

				<p>Конференція приймає рішення стосовно лише змін і доповнень до Статуту, вказаних у повідомленні про скликання Конференції.</p>
		</li>
		<li>3міни і доповнення до Статуту набирають чинності з моменту державної реєстрації таких змін в установленому законодавством порядку.</li>
		<h5>
			XІ. ПРИПИНЕННЯ СПІЛКИ
		</h5>
		<li> Припинення Спілки проводиться шляхом її реорганізації шляхом приєднання до іншої громадської організації, або ліквідації (саморозпуску) у порядку, встановленому законодавством України.</li>
		<li> Підстави і порядок ліквідації (примусового розпуску) або заборони Спілки, а також звернення стягнень на майно Спілки визначаються законодавством України.</li>
		<li> Ліквідація (саморозпуск) Спілки здійснюється на підставі рішення Конференції, яке визначає порядок і строки ліквідації Спілки згідно з законодавством України.</li>
		<li> У випадку ліквідації (саморозпуску) Спілки Правління виконує функції ліквідаційної комісії, якщо Конференція не визначить інший уповноважений орган. Ліквідаційна комісія виконує функції управління справами Спілки з дня її призначення, виступає в судах і виконує інші дії від імені Спілки, що припиняється. Ліквідаційна комісія несе солідарну майнову відповідальність за шкоду, заподіяну Спілці або третім особам.</li>
		<li> Ліквідаційна комісія передає активи, що залишилися після задоволення вимог кредиторів Спілки, одному або кільком громадським об’єднанням, що зареєстровані в Україні, або в доход бюджету.</li>
		<li> Спілка є такою, що припинилася, з дати внесення відповідного запису до Єдиного державного реєстру юридичних осіб та фізичних осіб-підприємців.</li>
	</ol>
</div>